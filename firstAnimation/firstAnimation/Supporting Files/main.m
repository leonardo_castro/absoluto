//
//  main.m
//  firstAnimation
//
//  Created by Leo Castro on 5/12/14.
//  Copyright Daniel Castro 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
