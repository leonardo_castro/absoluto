//
//  SoundSprite.h
//  Absoluto
//
//  Created by Leo Castro on 5/5/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SoundSprite : SKSpriteNode

 @property (nonatomic)NSString* spriteSound;     // used to store the sound of the sprite


@end
