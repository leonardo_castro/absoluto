//
//  AppDelegate.h
//  Absoluto
//
//  Created by Leo Castro on 5/4/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
