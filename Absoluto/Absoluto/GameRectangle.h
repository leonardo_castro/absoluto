//
//  GameRectangle.h
//  Absoluto
//
//  Created by Leo Castro on 5/4/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface GameRectangle : NSObject

@property (nonatomic)SKSpriteNode* gameSprite;
@property (nonatomic)UIColor* rectangleColor;
@property (nonatomic)NSString* rectangleTone;

-(SKSpriteNode *)spriteWidth:(float)width andHeight:(float)height withColor:(SKColor *)color positionX:(float)x positionY:(float)y;

-(void)setTone:(NSString *)toneURL;;

-(NSString *)getTone;

-(SKSpriteNode *)getSprite;


@end
