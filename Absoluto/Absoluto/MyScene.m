//
//  MyScene.m
//  Absoluto
//
//  Created by Leo Castro on 5/4/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import "MyScene.h"
#import "SoundSprite.h"

@interface MyScene()
{
    NSTimer* timer;
    NSTimer* backgroundOneTimer;
    NSTimer* backgroundTwoTimer;
    NSTimer* backgroundThreeTimer;
    NSTimer* backgroundFourTimer;
    
    NSArray* arrayOfTones;
    NSArray* arrayOfSprites;
    NSArray* arrayForTopAndBottomSprites;
}

//TONES
@property (nonatomic)NSString* redTone;
@property (nonatomic)NSString* blueTone;
@property (nonatomic)NSString* yellowTone;
@property (nonatomic)NSString* orangeTone;

//SPRITES
@property (nonatomic)SoundSprite* blueSprite;
@property (nonatomic)SoundSprite* redSprite;
@property (nonatomic)SoundSprite* yellowSprite;
@property (nonatomic)SoundSprite* orangeSprite;


@property (nonatomic)SoundSprite* playButton;
@property (nonatomic)SoundSprite* correctChoice;


// used to figure out which sprite was touched, and for sound purposes.
@property (nonatomic) SoundSprite* topSprite;
@property (nonatomic) SoundSprite* bottomSprite;
@property (nonatomic) NSString* randomTone;
@property (nonatomic) SoundSprite* wrongChoice;
@property (nonatomic) SoundSprite* firstBkgd;
@property (nonatomic) SKSpriteNode* checkmark;
@property (nonatomic) SKSpriteNode* ex;

@end

@implementation MyScene

-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
        {            
            [self loadSpritesAndArrays];
            self.backgroundColor = [SKColor colorWithWhite: 1.0 alpha: 1.0];
            [self loadBackgrounds];
        }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint location = [[touches anyObject] locationInNode:self];
    NSArray* nodes = [self nodesAtPoint:location];
    
    for (SKSpriteNode* node in nodes)
    {
        // PLAY touched.
        if (node.position.y == 200)
        {
            [self.view setUserInteractionEnabled:NO];
            [self fadeAndDeletePlaySprite];
            
            timer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                                     target: self
                                                   selector: @selector(startNewGame)
                                                   userInfo: nil
                                                    repeats: NO];
        }
        
        // TOP sprite touched.
        if(node.position.y == (self.frame.size.height/2.0) + 50)
        {
            if (self.topSprite.spriteSound == self.correctChoice.spriteSound)
                {
                    NSLog(@"Correct!");
                   [self playCheckmark];
                   [self clearSceneAndStartNewGame];
                }
            else if (self.topSprite.spriteSound != self.correctChoice.spriteSound)
                {
                    NSLog(@"Wrong");
                    [self playEx];
                    [self clearSceneAndStartNewGame];
                }
        }
        
        // BOTTOM sprite touched.
        if(node.position.y == (self.frame.size.height/2.0) - 50)
        {
            if (self.bottomSprite.spriteSound == self.correctChoice.spriteSound)
                {
                    NSLog(@"Correct!");
                    [self playCheckmark];
                    [self clearSceneAndStartNewGame];
                }
            else if (self.bottomSprite.spriteSound != self.correctChoice.spriteSound)
                {
                    NSLog(@"Wrong!");
                    [self playEx];
                    [self clearSceneAndStartNewGame];
                }
        }
        
    }
}

// BACKGROUNDS
-(void)loadBackgrounds
{
     backgroundOneTimer= [NSTimer scheduledTimerWithTimeInterval: 1
                                                         target: self
                                                       selector: @selector(displayBackgroundOne)
                                                       userInfo: nil
                                                        repeats: NO];
    
    
    backgroundTwoTimer= [NSTimer scheduledTimerWithTimeInterval: 4
                                                         target: self
                                                       selector: @selector(displayBackgroundTwo)
                                                       userInfo: nil
                                                        repeats: NO];
    
    
    backgroundThreeTimer= [NSTimer scheduledTimerWithTimeInterval: 7
                                                           target: self
                                                         selector: @selector(displayBackgroundThree)
                                                         userInfo: nil
                                                          repeats: NO];
  
    
    backgroundFourTimer= [NSTimer scheduledTimerWithTimeInterval: 10
                                                          target: self
                                                        selector: @selector(displayBackgroundFour)
                                                        userInfo: nil
                                                         repeats: NO];

    timer= [NSTimer scheduledTimerWithTimeInterval: 13
                                                          target: self
                                                        selector: @selector(loadPlayButton)
                                                        userInfo: nil
                                                         repeats: NO];

}

-(void)displayBackgroundOne
{
    SoundSprite* sprite = [self createBackgroundSprite: self.firstBkgd withImage:@"blueBackground"];
    sprite.alpha = 0;
    
    // ACTIONS:
    SKAction* fadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction* fadeOut = [SKAction fadeOutWithDuration:1.5];
    
    // SEQUENCES:
    // Create the sequence
    SKAction* mySequence = [SKAction sequence:@[fadeIn,fadeOut]];
    
    sprite.position = CGPointMake(250, 250);
    [self addChild:sprite];
    
    // RUN ACTION SEQUENCES:
    [sprite runAction:mySequence];
    
    SKAction* playTone = [SKAction playSoundFileNamed: self.blueTone waitForCompletion: NO];
    [self runAction: playTone];
}

-(void)displayBackgroundTwo
{
    SoundSprite* sprite = [self createBackgroundSprite: self.firstBkgd withImage:@"redBackground"];
    sprite.alpha = 0;
    // ACTIONS:
    SKAction* fadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction* fadeOut = [SKAction fadeOutWithDuration:1.5];
    
    // SEQUENCES:
    // Create the sequence
    SKAction* mySequence = [SKAction sequence:@[fadeIn,fadeOut]];
    
    sprite.position = CGPointMake(250, 250);
    [self addChild:sprite];
    
    // RUN ACTION SEQUENCES:
    [sprite runAction:mySequence];
    
    SKAction* playTone = [SKAction playSoundFileNamed: self.redTone waitForCompletion: NO];
    [self runAction: playTone];
}

-(void)displayBackgroundThree
{
    SoundSprite* sprite = [self createBackgroundSprite: self.firstBkgd withImage:@"yellowBackground"];
    sprite.alpha = 0;
    // ACTIONS:
    SKAction* fadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction* fadeOut = [SKAction fadeOutWithDuration:1.5];
    
    // SEQUENCES:
    // Create the sequence
    SKAction* mySequence = [SKAction sequence:@[fadeIn,fadeOut]];
    
    sprite.position = CGPointMake(250, 250);
    [self addChild:sprite];
    
    // RUN ACTION SEQUENCES:
    [sprite runAction:mySequence];
    
    SKAction* playTone = [SKAction playSoundFileNamed: self.yellowTone waitForCompletion: NO];
    [self runAction: playTone];
    
}

-(void)displayBackgroundFour
{
    SoundSprite* sprite = [self createBackgroundSprite: self.firstBkgd withImage:@"orangeBackground"];
    sprite.alpha = 0;
    // ACTIONS:
    SKAction* fadeIn = [SKAction fadeInWithDuration:1.0];
    SKAction* fadeOut = [SKAction fadeOutWithDuration:1.5];
    
    // SEQUENCES:
    // Create the sequence
    SKAction* mySequence = [SKAction sequence:@[fadeIn,fadeOut]];
    
    sprite.position = CGPointMake(250, 250);
    [self addChild:sprite];
    
    // RUN ACTION SEQUENCES:
    [sprite runAction:mySequence];
    
    SKAction* playTone = [SKAction playSoundFileNamed: self.orangeTone waitForCompletion: NO];
    [self runAction: playTone];
    

}

-(SoundSprite *)createBackgroundSprite:(SoundSprite *)sprite withImage:(NSString *)imageName
{
    sprite = [SoundSprite spriteNodeWithImageNamed:imageName];
    return sprite;
}

-(void)loadSpritesAndArrays
{
    // CHECKMARK SPRITE
    self.checkmark = [[SKSpriteNode alloc] initWithImageNamed:@"checkmark.png"];
    self.checkmark.position = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0);
    self.checkmark.alpha = 0;
    
    
    // EX SPRITE
    self.ex = [[SKSpriteNode alloc] initWithImageNamed:@"ex.png"];
    self.ex.position = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0);
    self.ex.alpha = 0;
    
    
    // PLAY SPRITE.
    self.playButton =[ [SoundSprite alloc]
                      initWithImageNamed:@"play.png" ];

    self.playButton.position = CGPointMake(self.frame.size.width/2.0, 200);
    self.playButton.alpha = 0;
    
    
    // BLUE SPRITE
    self.blueSprite = [[SoundSprite alloc] initWithColor:[SKColor blueColor]
                                                    size: CGSizeMake(200, 80)];
    [self.blueSprite setSpriteSound:self.blueTone];
    
    // RED SPRITE
    self.redSprite = [[SoundSprite alloc] initWithColor:[SKColor redColor]
                                                   size: CGSizeMake(200, 80)];
    [self.redSprite setSpriteSound:self.redTone];
    
    
    // YELLOW SPRITE
    self.yellowSprite = [[SoundSprite alloc] initWithColor:[SKColor yellowColor]
                                                      size: CGSizeMake(200, 80)];
    [self.yellowSprite setSpriteSound:self.yellowTone];
    
    
    // ORANGE SPRITE
    self.orangeSprite = [[SoundSprite alloc] initWithColor:[SKColor orangeColor]
                                                      size: CGSizeMake(200, 80)];
    [self.orangeSprite setSpriteSound:self.orangeTone];
    
    
    // TOP SPRITE
    self.topSprite = [[SoundSprite alloc] initWithColor:[SKColor greenColor]
                                                   size: CGSizeMake(200, 80)];
    
    
    // BOTTOM SPRITE
    self.bottomSprite = [[SoundSprite alloc] initWithColor:[SKColor greenColor]
                                                      size: CGSizeMake(200, 80)];
    
    // ARRAYS
    self.redTone = @"redTone.m4a";
    self.blueTone = @"blueTone.m4a";
    self.yellowTone = @"yellowTone.m4a";
    self.orangeTone = @"orangeTone.m4a";
    
    arrayOfTones = [NSArray arrayWithObjects:
                    self.redTone,           // A-Note
                    self.blueTone,          // E-note
                    self.yellowTone,        // C-note
                    self.orangeTone,        // G -note
                    nil];
    
    
    arrayOfSprites = [NSArray arrayWithObjects:
                      self.redSprite,
                      self.blueSprite,
                      self.yellowSprite,
                      self.orangeSprite,
                      nil];
    
    // Array used to enable random selection of button.
    arrayForTopAndBottomSprites = [NSArray arrayWithObjects: self.topSprite,
                                                             self.bottomSprite,
                                                             nil];
    
}

-(void)startNewGame
{
    [self spriteExitRight:self.playButton];
    [self playRandomTone];
}

-(void)playRandomTone
{
    int randomToneSelector = (arc4random() % 4);
    self.randomTone = arrayOfTones[randomToneSelector];
    self.correctChoice.spriteSound = [self randomTone];
    [self toneToSpriteCreator:self.randomTone];
}

-(void)toneToSpriteCreator:(NSString*)tone
{
    if([tone isEqualToString:self.redTone])
    {
        self.correctChoice = self.redSprite;
        self.correctChoice.spriteSound = @"correctChoice";
    }
    else if([tone isEqualToString:self.blueTone])
    {
        self.correctChoice = self.blueSprite;
        self.correctChoice.spriteSound = @"correctChoice";
    }
    else if([tone isEqualToString:self.yellowTone])
    {
        self.correctChoice = self.yellowSprite;
        self.correctChoice.spriteSound = @"correctChoice";
    }
    else if([tone isEqualToString:self.orangeTone])
    {
        self.correctChoice = self.orangeSprite;
        self.correctChoice.spriteSound = @"correctChoice";
    }
    
    [self setupUserButtonChoices];
}

-(void)setupUserButtonChoices
{
    [self.view setUserInteractionEnabled: NO];
    
    int randomButtonSelector = arc4random() % 2;
    int randomSpriteSelector = arc4random() % 4;
    
    self.wrongChoice = arrayOfSprites[randomSpriteSelector];
    
    
    // OPTION 1
    if (randomButtonSelector == 1)
       {
        // SETUP CORRECT CHOICE - BOTTOM
         self.bottomSprite.spriteSound = self.correctChoice.spriteSound;
        [self positionSpriteAtBottom: self.correctChoice];
        
        if ([self.wrongChoice.spriteSound isEqualToString: self.correctChoice.spriteSound ])
            {
                while ([self.wrongChoice.spriteSound isEqualToString: self.correctChoice.spriteSound])
                      {
                          randomSpriteSelector = arc4random() % 4;
                          self.wrongChoice = arrayOfSprites[randomSpriteSelector];
                          NSLog(@"OPTION 1 - while loop");
                      }
            
                [self positionSpriteAtTop: self.wrongChoice];
            
                 NSLog(@"OPTION 1 - left WHILE - CORRECT=BOTTOM");
            }
        else
        {
            NSLog(@"OPTION 1 - CORRECT BOTTOM - skipped while - directly to ELSE");
            [self positionSpriteAtTop:self.wrongChoice];
        }
        }
    
    // OPTION 2
    else if (randomButtonSelector == 0)
    {
        // Setup CORRECT CHOICE - TOP POSITION
        self.topSprite.spriteSound = self.correctChoice.spriteSound;
       [self positionSpriteAtTop: self.correctChoice];
        
       if ([self.wrongChoice.spriteSound isEqualToString: self.correctChoice.spriteSound ])
           {
               while ([self.wrongChoice.spriteSound isEqualToString: self.correctChoice.spriteSound])
                     {
                         randomSpriteSelector = arc4random() % 4;
                         self.wrongChoice = arrayOfSprites[randomSpriteSelector];
                         NSLog(@"OPTION 2 - while loop");
                     }
            
               
               [self positionSpriteAtBottom: self.wrongChoice];
            
                NSLog(@"OPTION 2 - left WHILE - Correct=TOP");
               
           }
        else
           {
               [self positionSpriteAtBottom:self.wrongChoice];
               NSLog(@"OPTION 2 - ELSE - CORRECT= TOP");
           }
    }
    
    
    // ANIMATE AND PLAY SOUND
    [self moveToCenter: self.wrongChoice];
    [self moveToCenter: self.correctChoice];
    [self addChild: self.wrongChoice];
    [self addChild: self.correctChoice];
    
    SKAction* playTone = [SKAction playSoundFileNamed: self.randomTone waitForCompletion: NO];
    [self runAction: playTone];
    
}

-(void)clearSceneAndStartNewGame
{
    [self.view setUserInteractionEnabled:NO];
    [self spriteExitLeft:self.correctChoice];
    [self spriteExitRight:self.wrongChoice];
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.5
                                             target: self
                                           selector: @selector(removeButtons)
                                           userInfo: nil
                                            repeats: NO];
    
    
    [self loadSpritesAndArrays];
    
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                             target: self
                                           selector: @selector(startNewGame)
                                           userInfo: nil
                                            repeats: NO];
}

-(void)removeButtons
{
    [self.correctChoice removeFromParent];
    [self.wrongChoice removeFromParent];
}

// ANIMATIONS ****************************************************************
-(void) playCheckmark
{
    NSLog(@"In checkmark method");
    SKAction* fadeIn = [SKAction fadeInWithDuration:0.5];
    SKAction* fadeOut = [SKAction fadeOutWithDuration:0.5];
    SKAction* wait = [SKAction waitForDuration:0.5];
    
    // SEQUENCES:
    // Create the sequence
    SKAction* mySequence = [SKAction sequence:@[fadeIn,wait,fadeOut]];
    
    [self addChild:self.checkmark];
   
    // RUN ACTION SEQUENCES:
    [self.checkmark runAction:mySequence];
}

-(void) playEx
{
    SKAction* fadeIn = [SKAction fadeInWithDuration:0.5];
    SKAction* fadeOut = [SKAction fadeOutWithDuration:0.5];
    SKAction* wait = [SKAction waitForDuration:0.5];
    
    // SEQUENCES:
    // Create the sequence
    SKAction* mySequence = [SKAction sequence:@[fadeIn,wait,fadeOut]];
   
    [self addChild:self.ex];
    
    // RUN ACTION SEQUENCES:
    [self.ex runAction:mySequence];
}

-(void)loadPlayButton
{
    //[self addChild:self.playButton];
    NSLog(@"In loadPlayButton");

    SKAction* fadeIn = [SKAction fadeInWithDuration:0.5];
    [self.playButton runAction:fadeIn];
    [self addChild:self.playButton];
}

-(void)positionSpriteAtBottom:(SoundSprite *)spriteToPosition
{
    spriteToPosition.position = CGPointMake(600, (self.frame.size.height/2.0) - 50);
    [self.view setUserInteractionEnabled:YES];
}

-(void)positionSpriteAtTop:(SoundSprite *)spriteToPosition
{
    spriteToPosition.position = CGPointMake(-200, (self.frame.size.height/2.0) + 50);
    [self.view setUserInteractionEnabled:YES];
}

-(void)fadeAndDeletePlaySprite
{
    SKAction* fadeOut = [SKAction fadeOutWithDuration:0.5];
    [self.playButton runAction:fadeOut];
}

-(void)moveToCenter:(SKSpriteNode *)nodeToAnimate
{
    SKAction* moveToCenter = [SKAction moveToX: self.frame.size.width/2.0 duration: 0.5];
    // SKAction* mySequence = [SKAction sequence:@[moveToCenter]];
    [nodeToAnimate runAction:moveToCenter];
}

-(void)spriteExitLeft:(SKSpriteNode *)nodeToAnimate
{
    SKAction* moveLeft = [SKAction moveToX:-200 duration:0.5];
    // SKAction* mySequence = [SKAction sequence:@[moveToCenter]];
    [nodeToAnimate runAction:moveLeft];
}

-(void)spriteExitRight:(SKSpriteNode *)nodeToAnimate
{
    SKAction* moveRight = [SKAction moveToX:600 duration:0.5];
    // SKAction* mySequence = [SKAction sequence:@[moveToCenter]];
    [nodeToAnimate runAction:moveRight];
}

-(void)update:(CFTimeInterval)currentTime
{
    /* Called before each frame is rendered */
}

@end
