//
//  GameRectangle.m
//  Absoluto
//
//  Created by Leo Castro on 5/4/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import "GameRectangle.h"

@implementation GameRectangle

-(SKSpriteNode *)spriteWidth:(float)width andHeight:(float)height withColor:(SKColor *)color positionX:(float)x positionY:(float)y
{
    _gameSprite = [[SKSpriteNode alloc] initWithColor:color size:CGSizeMake(200, 80)];
    _gameSprite.position = CGPointMake(x,y);
    return _gameSprite;
}

-(void)setTone:(NSString *)toneURL;
{
    _rectangleTone = toneURL;
}

-(NSString *)getTone
{
    return _rectangleTone;
}

-(SKSpriteNode *)getSprite {
    return _gameSprite;
}

@end
