//
//  main.m
//  Absoluto
//
//  Created by Leo Castro on 5/4/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}