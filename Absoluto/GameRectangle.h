//
//  GameRectangle.h
//  Absoluto
//
//  Created by Leo Castro on 5/4/14.
//  Copyright (c) 2014 Daniel Castro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface GameRectangle : NSObject

@property (nonatomic)SKSpriteNode* rectangleSprite;
@property (nonatomic)UIColor* spriteColor;
@property (nonatomic)NSString* spriteTone;


-(SKSpriteNode *)spriteWidth:(float)width andHeight:(float)height;

-(void)spritePositionX:(float)x positionY:(float)y;


@end
